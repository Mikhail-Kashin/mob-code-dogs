import { v4 as uuidv4 } from 'uuid';
import dogs from '../data/dogs';

interface IDog {
  id: string;
  name: string;
  age: number;
  breed: string;
  goodDog: boolean;
  birthday: Date;
}

export default class Dog {
  id: string;
  name: string;
  age: number;
  breed: string;
  goodDog: boolean;
  birthday: Date;

  constructor({ name, age, breed, birthday }) {
    this.id = uuidv4();
    this.name = name;
    this.age = age;
    this.breed = breed;
    this.goodDog = false;
    this.birthday = new Date(birthday);
  }

  static getDogs() {
    return dogs;
  }

  static getDogById(id: string) {
    return dogs.find((dog) => dog.id === id);
  }

  static create(dogObject: IDog) {
    const { name, age, breed, birthday } = dogObject;
    if (!name) throw Error('Dog must have a name');
    if (!age) throw Error('Dog must have an age');
    if (!breed) throw Error('Dog must have a breed');
    if (!birthday) throw Error('Dog must have a birthday');
    const dog = new Dog(dogObject);
    dogs.push(dog);
    return dog;
  }
}
