import { Router } from 'express';
import Dog from '../models/Dog';
import dogs from '../data/dogs';

export default Router()
  .get('/', (req, res, next) => {
    try {
      const dogs = Dog.getDogs();
      res.status(200).json({ dogs, success: true });
    } catch (err) {
      next(err);
    }
  })

  .get('/:id', (req, res, next) => {
    try {
      const dog = Dog.getDogById(req.params.id);
      res.status(200).json({ dog, success: true });
    } catch (err) {
      next(err);
    }
  })

  .post('/', (req, res, next) => {
    try {
      const dog = Dog.create(req.body);
      res.status(200).json({ dog, success: true });
    } catch (err) {
      next(err);
    }
  });
