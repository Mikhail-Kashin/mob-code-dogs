/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import * as express from 'express';
import dogs from './app/controllers/dogs'

const baseUrl = '/api/v1'

const app = express();

app.use(express.json())

app.get(baseUrl, (req, res) => {
  res.json({ message: 'Welcome to dogs!' });
});

app.use(`${baseUrl}/dogs`, dogs)

const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}${baseUrl}`);
});
server.on('error', console.error);
